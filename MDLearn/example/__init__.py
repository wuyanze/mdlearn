
from .. import encoding, preprocessing, fp 

import pandas as pd 
import numpy as np 
import os.path

EXAMPLE_PATH = os.path.dirname(__file__)

def load_altp(target='Ei', load_names=False, transform=True):
    """ Load alkane data from alkanes with different temperature and pressure;
        target: 'Ei'/'Density'/'Compressibility'/'Expansion'/'Cp'
        load_names: True to return name of each molecule additionally
        transform: Only load selected fp into the training set.
    """

    df = pd.read_csv(os.path.join(EXAMPLE_PATH, 'altp/alkanes-npt-2018v3.txt'), sep=' ', index_col=False)
    encoder = encoding.FPEncoder(fp.AlkaneSKIndexer, 
        selector=None, 
        cache_filename=os.path.join(EXAMPLE_PATH, 'altp/alkanes-fp-cache.txt'),
        smiles_decoder='pybel')
    encoder.load_data(df['SMILES'], df['T'], df['P'])
    datax = encoder.encode()

    if transform:
        selector = np.ones(datax.shape[1], dtype=bool)
        selector[[14,15,17,18,19,20,21,22]] = False
        datax = datax[:, selector]

    if not load_names:
        return datax, df[target].as_matrix()[:, np.newaxis]
    else:
        names = []
        for name, t, p in zip(df['SMILES'], df['T'], df['P']):
            names.append('%s\t%.2e\t%.2e' % (name, t, p))

        return datax, df[target].as_matrix()[:, np.newaxis], np.array(names)

